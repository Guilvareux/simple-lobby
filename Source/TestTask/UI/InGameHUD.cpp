// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameHUD.h"

AInGameHUD::AInGameHUD()
{

}

void AInGameHUD::BeginPlay()
{
    Super::BeginPlay();
    if(ComboWidgetClass)
    {
        ComboWidget = CreateWidget<UComboWidget>(GetWorld(), ComboWidgetClass);
        if(ComboWidget)
        {
            ComboWidget->AddToViewport();
        }
    }

}

void AInGameHUD::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}

void AInGameHUD::DrawHUD()
{
    Super::DrawHUD();

}


void AInGameHUD::UpdateComboCount(int32 Value)
{
    if(ComboWidget)
    {
        ComboWidget->UpdateComboCount(Value);
    }

}


void AInGameHUD::ResetCombo()
{
    if(ComboWidget)
    {
        ComboWidget->ResetCombo();
    }

}